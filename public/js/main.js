$(function(){

    //Slider
	if ($('.fullSlider').length > 0) {
        $('.fullSlider').slick({
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 8000,
            arrows: false,
            dots: true,
        });
    };

    if ($('.basicArticlesSlider').length > 0) {
        $('.basicArticlesSlider').slick({
            centerMode: true,
            variableWidth: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 8000,
            arrows: false,
            dots: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: false,
                        variableWidth: false,
                    }
                },
            ]
        });
    };
    
    if ($('.sliderInfo').length > 0) {
    	$('.sliderInfo').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            dots: true,
            responsive: [
                            {
                              breakpoint: 1024,
                              settings: {
                                slidesToShow: 2,
                              }
                            },
                            {
                              breakpoint: 768,
                              settings: {
                                slidesToShow: 1,
                              }
                            },
                        ]
        });
    }

    //Nano scroll
    if ($('.js-nano-scroll').length > 0) {
	    $(".js-nano-scroll.nano").nanoScroller({
            preventPageScrolling: true,
            iOSNativeScrolling: true,
        });
	}

    //Horizontal scroll
    if($(".tabs--list").length > 0) {
        if ($(window).width() > 767){
            $(".tabs--list").mCustomScrollbar({
                axis:"x",
                theme:"dark",
                scrollbarPosition: "outside",
            });
        } else {
            $(".tabs--list").mCustomScrollbar("destroy");
        }
    }

    comprasionScroll();
    BlockHeight();

	//Mask phone
	if ($('.js-phone').length > 0) {
		$(".js-phone").mask("+(375) 99-999-99-99");
	}

	//Search close show
	$("body").on("input", ".js-search-write", function(){
		if($(this).val().length > 0) {
			$(this).siblings(".search--close").addClass("active");
		} else {
			$(this).siblings(".search--close").removeClass("active");
		}
	});

	//Click button close search
	$("body").on("click", ".js-close-search", function(e){
		e.preventDefault();
		$(this).siblings(".js-search-write").val("");
		$(this).removeClass("active");
        $(".searchResult, .js-block-search, .blackout, .js-search-mobile, .js-click-search").removeClass("active");
        $('body').removeClass('search-active hide');
	});

    //Click menu item
	$("body").on("click", ".js-click-menu", function(e){
		e.preventDefault();
		var submenu = $(this).siblings(".js-submenu");

		if($(this).hasClass("active")) {
			submenu.slideUp(300)
			$(this).removeClass("active")
			return false;
		}
		submenu.slideDown(300)
		$(this).addClass("active")
	});

    //Click menu header
	$("body").on("click", ".js-click-menuHeader", function(e){
		e.preventDefault();
        $(".headerButton-wrapp, .searchResult, .searchWrapp, .js-click-search, .js-block-search").removeClass("active");
        $('body').removeClass('search-active');
        if ($('.contentPage').hasClass('catalog--page')) {
            $(".catalogSideBar, .js-click-more").removeClass('active');
        }
        if($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(".blackout , .sideMenu").removeClass("active");
            $("body").removeClass("hide");
            if($('body').hasClass('lock-scroll')) {
                enableScroll();
            }
			return false;
		}

        $(".popup--item").removeClass("animation");
        setTimeout(function(){
            $('.popup--item').removeClass("show");
        }, 500);
        $(".block-sticky").removeClass("active");

		$(this).addClass("active");
		$(".blackout , .sideMenu").addClass("active");
        disableScroll();
        $("body").addClass("hide");
	});

	//Сlick outside the window
    $("body").on("click", function (e) {
        if (!$(".favorites--textIcon").is(e.target) &&
            $(".favorites--textIcon").has(e.target).length === 0) {
            $(".favorites--textIcon").removeClass("active");
        };
    });
	$("body").on("click", function (e) {
        if (!$(".js-click-filter, .filterList, .product--wrap, .js-click-popover, .js-popover, .js-click-menuHeader, .js-click-search, .js-block-search, .payment--button, .sideMenu , .catalogSideBar , .popup--item , .js-click-more, .js-click-popup").is(e.target) &&
            $(".js-click-filter, .filterList, .product--wrap, .js-click-popover, .js-popover, .catalogSideBar, .js-click-search, .js-block-search,  .payment--button, .js-click-menuHeader , .sideMenu , .js-click-more, .popup--item, js-click-popup").has(e.target).length === 0) {
			$(".js-click-filter, .js-block-search, .filterList, .js-click-popover, .js-popover, .text-input--label  , .searchResult , .blackout , .payment--wrapp , .sideMenu , .block-sticky , .js-click-menuHeader, .catalogSideBar, .js-click-more").removeClass("active");
			$(".popup--item, .filterList").removeClass("animation");
			$("body").removeClass("paymentActive");
            $("body").removeClass("hide");
            if($('body').hasClass('lock-scroll')) {
                enableScroll();
            }
            $('.product--wrap').removeClass('active-toggle');
            setTimeout(function(){
                $(".popup--item, .filterList").removeClass("show");
            }, 500)
        };
    });

    $("body").on("click", ".js-click-more", function(e){
    	e.preventDefault();
        if ($('.contentPage').hasClass('catalog--page')) {
            $('.sideMenu, .js-click-menuHeader').removeClass('active');
        }
    	if($(this).hasClass("active")){
    		$(this).removeClass("active");
    		$(".catalogSideBar , .blackout").removeClass("active");
            $("body").removeClass("hide");
            if($('body').hasClass('lock-scroll')) {
                enableScroll();
            }
    		return false;
    	}

        disableScroll();
        $("body").addClass("hide");
    	$(this).addClass("active");
    	$(".catalogSideBar , .blackout").addClass("active");
    });

    //Click zoom button catalog
    /*$("body").on("click", ".js-click-zoom", function(e){
    	e.preventDefault();

    	if($(this).hasClass("zoom-active")) {
    		$(this).removeClass("zoom-active");
    		$(".catalog--page").removeClass("zoom")
    		return false;
    	}

    	$(this).addClass("zoom-active");
    	$(".catalog--page").addClass("zoom")
    });*/

    //Click search header
    $("body").on("click",".js-click-search", function(e){
    	e.preventDefault();
    	$('.js-click-menuHeader, .sideMenu, .blackout').removeClass('active');
        if($(window).width() > 768){
            $(this).parent(".headerButton-wrapp").addClass("active");
        } else {
        	if($(this).hasClass("active")) {
        		$(this).removeClass("active");
                $(".js-search-mobile, .searchResult, .blackout").removeClass("active");
                $('body').removeClass('search-active');
        		return false;
        	}
        	$(this).addClass("active");
        	$('body').addClass('search-active');
            $(".js-search-mobile").addClass("active")
            $(".js-search-mobile").find('.search--input').focus();
        }
    });

    $("body").on("click",".js-click-search", function(e){
        e.preventDefault();
        if($(window).width() < 768 && $(this).hasClass('active')){
            $('.js-search-mobile').addClass('noScroll');
            setTimeout(function(){
                $('.js-search-mobile').removeClass('noScroll');
            }, 350)
        }
        $("body").removeClass("hide");
        if($('body').hasClass('lock-scroll')) {
            enableScroll();
        }
        $(".popup--item").removeClass("animation");
        $('.block-sticky').removeClass('active');
        setTimeout(function(){
            $(".popup--item").removeClass("show");
        }, 500)
    });

    $("body").on("click", ".js-close-searchDesktop", function(e){
        e.preventDefault();
        $(this).parents(".headerButton-wrapp").removeClass("active");
        $(".searchResult, .blackout").removeClass("active");
        $("body").removeClass("hide");
        if($('body').hasClass('lock-scroll')) {
            enableScroll();
        }
    });

    $("body").on("click",".js-click-popoverMore",function(e){
    	e.preventDefault();

    	if($(this).hasClass("active")) {
    		$(this).removeClass("active");
    		$(this).siblings(".js-block-popover").slideUp("300");
    		return false;
    	}

    	$(this).addClass("active");
    	$(this).siblings(".js-block-popover").slideDown("300");
    });

    $("body").on("click", ".js-click-popover", function(e){
        e.preventDefault();

        if($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).siblings(".js-popover").removeClass("active");
            return false;
        }

        $(this).addClass("active");
        $(this).siblings(".js-popover").addClass("active");
    });

    $("body").on("click", ".js-sticky-basket", function(e){
    	$(".block-sticky").addClass("active");
    });

    //Show pop-up and tabs click
    $("body").on("click", ".js-click-popup", function(e){
    	e.preventDefault();

    	var dataClick = $(this).attr('data-click'),
            blockShow  = $('.popup--item[data-block = '+ dataClick +']'),
            blockClick = $(this);
        if($(this).hasClass("remove-btn")) {
            dataIndex = $(this).closest("[data-index]").attr("data-index");
        }

        if($(window).width() > 768 && blockClick.hasClass("tabs--button")) {
                tabsButton(dataClick);
                return false;
        }
        if($(window).width() < 768 && blockClick.hasClass("userStatus--button")) {
            location.href = blockClick.prop('href');
            return false;
        }
        if($(window).width() < 768 && blockClick.hasClass("feedback--fixedButton")) {
            location.href = blockClick.prop('href');
            return false;
        }

        $('body').removeClass('search-active');
        $('.sideMenu, .js-click-menuHeader, .js-click-search, .js-search-mobile').removeClass('active');
        $(".blackout").addClass("active");
        blockShow.addClass("show");
        blockShow.addClass("animation");
        disableScroll();
        $("body").addClass("hide");

        if(dataClick == 'oneClick') {
            blockShow.find('input').focus();
        }

    	if(blockClick.hasClass("js-payment-popup")) {
            paymentPopapShow(blockClick);
    	}
        return false;
    });

    $("body").on("click", ".js-click-tabs", function(e){
        e.preventDefault();

        var dataClick = $(this).attr('data-click'),
            top = $('.tabs--list').offset().top - 45;
        tabsButton(dataClick);

        $('body,html').animate({scrollTop: top}, 1500);
    });

    //Hide pop-up
    $("body").on("click", ".js-popup-close, .blackout", function(e){
    	e.preventDefault();
        var popupHide = $(this).parent(".popup--item").attr("data-block");

    	$(this).parents(".popup--item").removeClass("animation");
    	setTimeout(function(){
    		$('.popup--item[data-block = '+ popupHide +']').removeClass("show");
    	}, 500)
    	if($(this).parents(".popup--item").hasClass("popup--payment")){
    		$("body").removeClass("paymentActive hide");
            setTimeout(function(){
    		  $(".payment--wrapp").removeClass("active");
            }, 500)
            if(!$(".popup--basket").hasClass("show")){
                $(".blackout , .block-sticky").removeClass("active");
                $("body").removeClass("hide");
                if($('body').hasClass('lock-scroll')) {
                    enableScroll();
                }
            }
    		return false;
    	}
    	$(".blackout , .block-sticky").removeClass("active");
        $("body").removeClass("hide");
        if($('body').hasClass('lock-scroll')) {
            enableScroll();
        }
    });

    $("body").on("click", ".js-change-payment", function(e){
    	e.preventDefault();

    	var dataClickLink = $(this).attr('data-content'),
        	blockShowContent  = $('.payment--wrapp[data-content = '+ dataClickLink +']');

    	$(this).parents(".payment--wrapp").removeClass("active");
    	blockShowContent.addClass("active");
    });

    //In customJs
    /*$('body').on('click', '.js-night-toggle', function(e){
        e.preventDefault();
        if($(this).siblings('input').prop('checked') == false) {
            $('body').addClass('night-page');
            $(this).siblings('input').prop('checked', true);
        } else {
            $('body').removeClass('night-page');
            $(this).siblings('input').prop('checked', false);
        }
        $('body').addClass('toggle-bg');
        setTimeout(function(){
            $('body').removeClass('toggle-bg');
        }, 300)
    });

   $('body').on('click', '.js-night-button', function (e) {
        e.preventDefault();
        var buttonToggle = $(this).siblings('.button-toggle').find('input');
       $('body').addClass('toggle-bg');
       setTimeout(function(){
           $('body').removeClass('toggle-bg');
       }, 300)
        if($(this).hasClass('sun') || $(window).width() < 768 && $('body').hasClass('night-page')) {
            $('body').removeClass('night-page');
            buttonToggle.prop('checked', false);
            return false;
        }
        $('body').addClass('night-page');
        buttonToggle.prop('checked', true);
    });*/

    if($('.night-page').length > 0) {
        $('.nightSwitch').find('input').prop('checked', true);
    }

    //Scroll window
	$(window).scroll(function(){
        heightCatalogSideBar();
        if($(window).width() < 768) {
            $('.feedback--fixed').addClass('opacity');
        }
    });
    $.fn.scrollEnd = function(callback, timeout) {
        $(this).scroll(function(){
            var $this = $(this);
            if ($this.data('scrollTimeout')) {
                clearTimeout($this.data('scrollTimeout'));
            }
            $this.data('scrollTimeout', setTimeout(callback,timeout));
        });
    };
    $(window).scrollEnd(function(){
        $('.feedback--fixed').removeClass("opacity");
    }, 1000);

    changeHeight();
    filterDesktopWidth();
    heightCatalogSideBar();
    //Resize window
    if ($(window).width() > 768){
        if ($('.tabs--content').length > 0) {
            $(".tabs--content .js-nano-scroll.nano").nanoScroller({ destroy: true });
        }
    }
    addressSlider();
    var observer = new MutationObserver(sliederObserve);
    var observer2 = new MutationObserver(maskObserv);
    observer.observe(document.querySelector('#root .right-content'), {childList: true});
    observer2.observe(document.querySelector('#root .right-content'), {childList: true});
	$(window).resize(function(){
        addressSlider();
        heightCatalogSideBar();
        comprasionScroll();
        BlockHeight();
        changeHeight();
        filterDesktopWidth();
        if($(".tabs--list").length > 0 ) {
            if ($(window).width() > 767){
                $(".tabs--list").mCustomScrollbar({
                    axis:"x",
                    theme:"dark",
                    scrollbarPosition: "outside",
                });
                $(".popup--item.tabs--content").css("display", "");
            } else {
                $(".tabs--list").mCustomScrollbar("destroy");
            }
        }
        if ($(window).width() > 768){
            $('body').removeClass('search-active');
			$(".catalog--page").removeClass("zoom");
            $(".js-search-mobile, .js-click-search").removeClass("active");
            $(".js-click-zoom").removeClass("zoom-active");

            if ($('.tabs--content').length > 0) {
                $(".tabs--content .js-nano-scroll.nano").nanoScroller({ destroy: true });
            }
        }
        if ($('.tabs--content').length > 0) {
            $(".tabs--content .js-nano-scroll.nano").nanoScroller({ destroy: false });
        }
        if($(window).width() >= 1144) {
			$(".js-click-more").removeClass("active");
		}
        //$(".popup--item").removeClass("show");
        //$(".blackout, .catalogSideBar, .js-click-more, .sideMenu, .js-click-menuHeader").removeClass("active");

	});

    if($(".catalogSideBar").length > 0) {
        $(".catalogSideBar").swipe({
            swipeStatus:function(event, phase, direction, distance, duration, fingers)
                {
                    if (phase=="move" && direction =="right" && $(window).width() < 767) {
                        $(".catalogSideBar , .blackout, .js-click-more").removeClass("active");
                        $("body").removeClass("hide");
                        if($('body').hasClass('lock-scroll')) {
                            enableScroll();
                        }
                        return false;
                    }
                    if (phase=="move" && direction =="left" && $(window).width() < 767) {
                        $(".catalogSideBar , .blackout, .js-click-more").addClass("active");
                        disableScroll();
                        $("body").addClass("hide");
                        return false;
                    }
                    if (phase=="move" && direction =="down") {
                        return false;
                    }
                    if (phase=="move" && direction =="up") {
                        return false;
                    }
                }
        });
    }

	$("body").on("click", ".js-close-btn", function () {
      var block = $(this).closest(".js-block-list");
      $(this).closest(".js-item").remove();
      if(block.find(".js-item").length == 0) {
        block.remove();
      }
      return false;
   });

    $("body").on("input, focus", ".js-focus-input",function(){
        var blockSearch = $(this).parents(".js-block-search"),
            blockSearchList = blockSearch.find(".searchResult")
        if (blockSearchList.children().length == 0) {
            blockSearchList.removeClass("active");
            return false;
        }
        blockSearch.addClass("active");
        blockSearchList.addClass("active");
        $('body').addClass('hide');
    })

    $("body").on("click", ".js-btn-plus, .js-btn-minus", function () {
        changeCounter($(this));
        return false;
    });

    $("body").on("change", ".js-counter--input", function () {
        if($(this).val().length < 1 || +$(this).val() < 1) {
            $(this).val(1);
        }
    });

     $("body").on("click", ".js-password-input--btn", function () {
        var parent = $(this).closest(".password-input--wrap");
        if(parent.hasClass("open")) {
            parent.removeClass("open").find(".password-input").attr("type", "password");
        } else {
            parent.addClass("open").find(".password-input").attr("type", "text");
        }
        return false;
    });

    $("body").on("click", ".js-validate", function () {
        validateForm($(this).closest(".inputsBlock"));
        $(this).removeClass("error-form").removeClass("success-form");
        if($(this).parents('.inputsBlock').find('.error').length > 0) {
            $(this).addClass("error-form");
        } else {
            $(this).addClass("success-form");
        }
        return false;
    });

    $("body").on("change", ".text-input, .select, .password-input", function () {
        if($(this).val() < 1) {
            $(this).parent().removeClass("full");
        } else {
            $(this).parent().addClass("full");
        }
        return false;
    });

    $('body').on('click', '.js-authorization-click', function (e) {
        e.preventDefault();
        var blockPopUp = $(this).parents('.popup--item');
        blockPopUp.removeClass('passwordRecovery-active');
        blockPopUp.removeClass('register-active');
        if($(this).hasClass('newAccount')) {
            blockPopUp.addClass('register-active');
            return false
        }
        if($(this).hasClass('passwordRecovery')) {
            blockPopUp.addClass('passwordRecovery-active');
            return false
        }
    });

    var dataIndex;

    $("body").on("click", ".js-confirm-remove", function(){
        var _this = $(this);
        removeBlock(dataIndex, _this);
        return false;
    });

    $("body").on("click", ".js-historyOrder--btn", function(){
        $(this).parent().toggleClass("checked").find(".historyOrderDetails").slideToggle();
        return false;
    });

    $("body").on("click", ".js-unfoldingList--title", function () {
        $(this).toggleClass("active").siblings(".unfoldingList--dropdown").slideToggle();
        return false;
    });

    $("body").on("click", ".js-click-radio", function(e){
        e.preventDefault();

        if($(this).hasClass("active")){
            $(this).removeClass("active");
            $(this).siblings(".js-block").slideUp("300");
            return
        }

        $(".js-click-radio").removeClass("active");
        $(".js-click-radio").siblings(".js-block").slideUp("300");
        $(this).addClass("active")
        $(this).siblings(".js-block").slideDown("300");
    });

    $("body").on("click", ".js-click-comparison", function(e){
        e.preventDefault();

        if($(this).hasClass("active")){
            $(this).removeClass("active");
            $(this).parent(".stickyBlockCompare").removeClass("active");
            return false;
        } 
        $(this).addClass("active");
        $(this).parent(".stickyBlockCompare").addClass("active");
    });

    $("body").on("click", ".js-product-click", function (e) {
       e.preventDefault();
       var blockWrap = $(this).parent(".product--wrap");
       if(blockWrap.hasClass("active-toggle")) {
           blockWrap.removeClass("active-toggle");
           //$(this).siblings(".js-block-hidden").slideUp("300");
           return
       }
       $('.product--wrap').removeClass('active-toggle');
        blockWrap.addClass("active-toggle");
       //$(this).siblings(".js-block-hidden").slideDown("300");
    });

    $('body').on('click', '.js-click-filter', function(e){
        e.preventDefault();
        var dataClick = $(this).attr('data-filter'),
        filterActive = $('.filterList--sheet[data-sheet = '+ dataClick +']');

        if($(this).hasClass('active') && ($(this).hasClass('filterMenu--button') || $(this).hasClass('item--button'))){
            $(this).removeClass('active');
            $('.filterList').removeClass('active');
            return
        }
        if ($(this).hasClass('filterMenu--button') || $(this).hasClass('item--button')) {
            $('.js-click-filter').removeClass('active');
        }
        $('.filterList--sheet').removeClass('active');
        filterActive.addClass('active');
        if ($(window).width() < 768) {
            $(this).siblings('.filterList').addClass('show animation');
            disableScroll();
            $("body").addClass("hide");
            $(".blackout").addClass("active");

            return false;
        }
        if(!$(this).hasClass('filter--buttonTab')){
            $('.filterList').removeClass('active');
            $(this).addClass('active');
        }
        $(this).siblings('.filterList').addClass('active');
    });

    $('body').on('click', '.js-back-filter', function(e){
       e.preventDefault();
       $('.filterList--sheet').removeClass('active');
       $('.filterList--sheet[data-sheet =filter]').addClass('active');
    });

    $('body').on('click', '.js-close-filter', function(e){
        e.preventDefault();
        if($(window).width() > 768) {
            $('.js-click-filter, .filterList, .filterList--sheet').removeClass('active');
            return false;
        }
        $(".filterList").removeClass("animation");
        $("body").removeClass("hide");
        $(".blackout").removeClass("active");
        if($('body').hasClass('lock-scroll')) {
            enableScroll();
        }
        setTimeout(function(){
            $(".filterList").removeClass("show");
        }, 500)
    });

    //LazyLoadInstance
    var lazyLoadInstance = new LazyLoad({
        elements_selector: "img"
    });
    if (lazyLoadInstance) {
        lazyLoadInstance.update();
    }

    $('body').on('click', '.js-dropdown-toggle', function(){
        $(this).find('.js-dropdown').toggle();
        return false;
    });

    $('body').on('click', '.setWarranty label', function(){/* 21295 21.01.2020 */
        if($(this).siblings("input[type='radio']").prop("checked")) {
            $(this).siblings("input[type='radio']").prop("checked", false);
            $(this).closest(".selectWarranty--list").siblings(".paymentNote--popoverService.active").removeClass("active");
            return false;
        }
    });

    $('body').on('change', '.js-setWarranty--radio', function(){/* 21295 21.01.2020 */
        if($(this).prop("checked")) {
            console.log($(this).prop("checked"));
            var dataClick = $(this).attr("data-click");
            $(this).closest(".selectWarranty--list").siblings(".paymentNote--popoverService.active").removeClass("active");
            $(this).closest(".selectWarranty--list").siblings(".paymentNote--popoverService[data-block='" + dataClick + "']").addClass("active");
        }
    });
});

function BlockHeight() {
    $(".js-block-content").each(function( i , el ){
        var heightContent = $(this).outerHeight(),
            blockContent = $(this).attr("data-content"),
            blockName  = $('.js-block-name[data-name = '+ blockContent +']'),
            blockNameHeight  = blockName.outerHeight();
        if ($(window).width() > 768){
            if ( heightContent != blockNameHeight ) {
                var maxHeight = Math.max( heightContent, blockNameHeight );
                $(this).outerHeight(maxHeight);
                blockName.outerHeight(maxHeight);
            }
        } else {
            $(this).outerHeight("auto");
            blockName.outerHeight("auto");
        }
    });
}

function changeCounter(btn) {
    var increaseStep = 1,
        input = btn.siblings(".selectCount--input");
    if(btn.hasClass("js-btn-minus")) {
        if(+input.val() === 2) {
            btn.addClass("disabled");
        }
        increaseStep = -1;
    }
    if(btn.hasClass("js-btn-plus") && btn.siblings(".selectCount--btn").hasClass("disabled")) {
        btn.siblings(".selectCount--btn").removeClass("disabled");
    }
    input.val(+input.val() + increaseStep);
    return false;
}

function tabsButton(dataClick) {
    var tabsShow = $('.tabs--content[data-block = '+ dataClick +']');
        $(".tabs--button").removeClass("tabs--active");
        $('.product--button[data-click = '+ dataClick +']').addClass("tabs--active");
        $(".tabs--content").removeClass("active");
        tabsShow.addClass("active").show();
}

function paymentPopapShow(blockClick) {
    var dataClickLink = blockClick.attr('data-content'),
        blockShowContent  = $('.payment--wrapp[data-content = '+ dataClickLink +']');
    if($('.popup--basket').hasClass('show')) {
        $("body").addClass("paymentActive");
    }
    blockShowContent.addClass("show");
    blockShowContent.addClass("active");
}

function validateForm(form) {
    var valide = true;
    form.find("*[data-required]:not('.disabled')").each(function () {
        switch ($(this).attr("data-name")) {
            case "length":
                valide = lengthValidate($(this).find("input, textarea"));
                break;
            case "email":
                valide = emailValidate($(this).find("input"));
                break;
            case "password":
                valide = passwordValidate($(this).find("input"));
                break;
        }
        if(!valide) {
            $(this).removeClass("success").addClass("error");
        } else if(valide && valide !== "break") {
            $(this).removeClass("error").addClass("success");
        }
    });
    return valide;
}

function lengthValidate(field) {
    if(field.val().length < 1) {
        return false;
    }
    return true;
}

function emailValidate(field) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i),
        emailAddress = field.val();
    return pattern.test(emailAddress);
}

function passwordValidate(field) {
    var passField = field.parent().siblings(".password-input--wrap").find("input");
    if(field.val() !== passField.val()) {
        return false;
    }
    return true;
}

function addressSlider() {
    if ($('.addressSaved--list').length > 0) {
        var blockWidth = $('.addressSaved--list').innerWidth(),
            blockItem = $('.addressSaved--item'),
            blockItemsWidth = 0;
        blockItem.each(function(index, el){
            blockItemsWidth = blockItemsWidth + $(el).innerWidth()
        });
        if (blockItemsWidth > blockWidth && !($('.addressSaved--list').hasClass('slick-slider'))) {
            $('.addressSaved--list').slick({
                variableWidth: true,
                infinite: false,
                arrows: false,
                dots: true,
            });
            sliderIsLive = true;
            $('.addressSaved--item .user--desc, .addressSaved--item .location--desc').css("-webkit-line-clamp", "2");
        }
        if (blockItemsWidth < blockWidth && $('.addressSaved--list').hasClass('slick-slider')) {
            $('.addressSaved--list').slick('unslick')
            sliderIsLive = false;
        }
    };
}

function removeBlock(index, item) {
    var block = $("[data-index='"+ index +"']"),
        blockWrap = block.closest(".slick-initialized");
    if(blockWrap.length > 0) {
        blockWrap.slick('slickRemove', block.index());
    } else {
        block.remove();
    }
    item.closest(".popup--item").removeClass("animation");
    $("body").removeClass("paymentActive");
    $("body").removeClass("hide");
    if($('body').hasClass('lock-scroll')) {
        enableScroll();
    }
    setTimeout(function(){
        item.closest(".popup--item").removeClass("show");
    }, 500);
    $(".blackout , .block-sticky").removeClass("active");
}

//Scroll catalog
function heightCatalogSideBar() {
    var footerPosition = $(".footer").offset().top,
        top_of_element = $('.section--infoHeader').outerHeight() + $(".section--infoHeader").offset().top,
        windowScroll = $(window).scrollTop(),
        windowHeight = $(window).height(),
        footer_height = footerPosition - (windowHeight + windowScroll),
        block_height_mobile = windowHeight,
        block_height_desctop = windowHeight - 64;
    if(top_of_element > windowScroll) {
        block_height_desctop = block_height_desctop - (top_of_element - windowScroll) + 48;
    }
    if (footer_height < 0) {
        block_height_mobile = footer_height + block_height_mobile,
        block_height_desctop = footer_height + block_height_desctop;
    }
    if ($(window).width() > 768){
       $(".catalogSideBar").css("max-height", 'none');
        $(".sideBar--list").css("max-height", block_height_desctop);
    } else {
        $(".catalogSideBar").css("max-height", block_height_mobile);
        $(".sideBar--list").css("max-height", 'none');
    }
}

function comprasionScroll(removeClick) {
    if ($(".comparisonProduct").length > 0) {
        var widthBlockWrap = $(".comparisonProduct").width() - 264,
            itemBlock = $(".comparison--listItem .comparison--itemImgList"),
            item = itemBlock.find(".comparison--itemImg ").length,
            itemLength = item/itemBlock.length,
            itemLengthWindth = itemLength*212;
        if(removeClick == true) {
            $(".comparison--listItemWrap").removeClass('item-'+(itemLength+1)+'');
        }
        if ($(window).width() < 768 || widthBlockWrap > itemLengthWindth) {
            $(".comparison--listItemWrap").mCustomScrollbar("destroy").addClass('item-'+itemLength+'');
            if (($(window).width() < 768) == true) {
                $(".comparison--listItemWrap").removeClass('item-'+itemLength+'');
            }
        } else {
            $(".comparison--listItemWrap").mCustomScrollbar({
                axis:"x",
                theme:"dark",
                scrollbarPosition: "outside",
            }).removeClass('item-'+itemLength+'');
        }
    }
}

//PhotoSize
(function($) {
    var $pswp = $('.pswp')[0];
    var image = [];

    $('.gallery--picture').each( function() {
        var $pic     = $(this),
            getItems = function() {
                var items = [];
                $pic.find('a').each(function() {
                    var $href   = $(this).attr('href'),
                        $size   = $(this).data('size').split('x'),
                        $width  = $size[0],
                        $height = $size[1];

                    var item = {
                        src : $href,
                        w   : $width,
                        h   : $height
                    }

                    items.push(item);
                });
                return items;
            }

        var items = getItems();

        $.each(items, function(index, value) {
            image[index]     = new Image();
            image[index].src = value['src'];
        });

        $pic.on('click', '.js-click-picture', function(event) {
            event.preventDefault();
            
            var $index = $(this).index();
            var options = {
                index: $index,
                bgOpacity: 0.8,
                showHideOpacity: true
            }

            var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
            lightBox.init();
        });
    });
})(jQuery);

function loaderStart(block) {
    if (block == undefined) {
        var block = 'body';
        $(''+ block +'').addClass('loaderArea active-body');
    } else {
        $(''+ block +'').addClass('loaderArea active-loader');
    }
}

function loaderEnd(block) {
    if (block == undefined) {
        var block = 'body';
        $(''+ block +'').removeClass('active-body');
        setTimeout(function(){
            $(''+ block +'').removeClass('loaderArea');
        }, 350)
    } else {
        $(''+ block +'').removeClass('active-loader');
        setTimeout(function(){
            $(''+ block +'').removeClass('loaderArea');
        }, 350)
    }
}

function changeHeight() {
    var heightWindow = $(window).innerHeight() * 0.01;
    $('html').css('--vh', `${heightWindow}px`);
}

function disableScroll(elem){
    var lastScrollTop = $(window).scrollTop();
    $('body').addClass("lock-scroll");
    $('body').css("top", "" + ((-(Math.round(lastScrollTop)))+48) + "px");
}
function enableScroll(){
    var lastScrollTop = Number($('body').css("top").match(/[0-9]/g).join("")),
        lastScrollTopNumber = Number($('body').css("top").split('px').join(''));
    if ((lastScrollTop) >= 0 ) {
        lastScrollTopNumber = 48 - lastScrollTopNumber
    } else {
        lastScrollTopNumber = lastScrollTopNumber + 48
    }
    $('body').css("top", "0px");
    $('.lock-scroll').removeClass("lock-scroll");
    $('body,html').scrollTop(lastScrollTopNumber);
}

function filterDesktopWidth() {
    if($(window).width() > 768 && $('.filterDesktop').length > 0) {
        var blockWrappWidth = $('.filterDesktop').outerWidth(true)-10,
            itemWidth = 0;
        $('.filterDesktop--item').removeClass('hidden');
        $('.filterDesktop .filterDesktop--item').each(function(index , element){
            itemWidth = itemWidth + $(element).outerWidth(true);
            console.log(itemWidth, blockWrappWidth)
            if (itemWidth > blockWrappWidth) {
                $(element).addClass('hidden');
            }
        });

    }
}

function sliederObserve() {
    if(document.querySelector('#root .addressSaved--list')) {
        var sliederObserver = new MutationObserver(addressSlider);
        sliederObserver.observe(document.querySelector('#root .addressSaved--list'), {childList: true});
    }
}

function maskObserv() {
    if(document.querySelector('#root .inputsBlock--list') && $('.js-phone').length > 0) {
        $(".js-phone").mask("+(375) 99-999-99-99");
    }
}
