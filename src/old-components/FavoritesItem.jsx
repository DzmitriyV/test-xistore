import React, {Component} from 'react';

class FavoritesItem extends Component {
    render(){
        let {index, name, link, price, oldPrice, popupIndex, imgName} = this.props;
        let oldPriceBlock = (
            <div className="old-price priceSmall priceSmall--old">
                {oldPrice[0]}<sup>{oldPrice[1]}</sup>
            </div>
        );
        return(
                <div className="favoritesList--item cardProduct" data-index={index}>
                    <div className="cardProduct--img">
                        <a href={link}>
                            <picture>
                                <source type="image/webp" srcSet={`http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}.webp 1x,
                                                    http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}x2.webp 2x`}/>
                                <img srcSet={`http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}x2.png 2x" src="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}.png`}
                                     alt="Просто изображение"/>
                            </picture>
                        </a>
                    </div>
                    <div className="cardProduct--content">
                        <div className="cardProduct--name">
                            <a href={link}>{name}</a>
                        </div>
                        <div className="cardProduct--price">
                            <div className="new-price priceSmall priceSmall--discount">
                                {price[0]}<sup>{price[1]}</sup>
                            </div>
                            {oldPrice ? oldPriceBlock : ""}
                        </div>
                        <a className="cardProduct--trash js-click-popup remove-btn" data-click={popupIndex} href="javascript:void(0);"></a>
                    </div>
                </div>
        )
    }
}

export default FavoritesItem;