import React, {Component} from 'react';
import CardProduct from "../CardProduct";

class HistoryItem extends Component {
    render(){
        let {orderNumber, orderDate, orderStatus, basket, basketCount, summ, discount, deliveryPrice, totalSumm} = this.props;
        let items = basket.map(item => {
            return (
                <CardProduct
                    key={item.index}
                    index={item.index}
                    name={item.name}
                    link={item.link}
                    price={item.price}
                    oldPrice={item.oldPrice}
                    imgName={item.imgName}
                    count={item.count}
                    totalPrice={item.totalPrice}
                />
            );
        });
        return(
            <div className="historyOrder--item">
                <a className="historyOrder--btn js-historyOrder--btn" href="#">
                    <div className="orderNumber">
                        <strong>Заказ #{orderNumber}</strong> от {orderDate}
                    </div>
                    <div className="orderStatus">
                        <div className="statusImg">
                            <img src={`http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${orderStatus.img}.svg`}/>
                        </div>
                        <div className="statusText">
                            {orderStatus.text}
                        </div>
                    </div>
                </a>
                <div className="historyOrderDetails">
                    {items}
                    <div className="orderResult--item">
                        <div className="orderResult--item-name">Количество продуктов:</div>
                        <div className="orderResult--item-value">{basketCount}</div>
                    </div>
                    <div className="orderResult--item">
                        <div className="orderResult--item-name">Общая сумма:</div>
                        <div className="orderResult--item-value priceSmall">{summ[0]}<sup>{summ[1]}</sup></div>
                    </div>
                    <div className="orderResult--item">
                        <div className="orderResult--item-name">Скидка:</div>
                        <div className="orderResult--item-value priceSmall priceSmall--discount">{discount[0]}<sup>{discount[1]}</sup></div>
                    </div>
                    <div className="orderResult--item">
                        <div className="orderResult--item-name">Стоимость доставки:</div>
                        <div className="orderResult--item-value priceSmall">{deliveryPrice[0]}<sup>{deliveryPrice[1]}</sup></div>
                    </div>
                    <div className="orderResult--item">
                        <div className="orderResult--item-name">Итоговая стоимость:</div>
                        <div className="orderResult--item-value priceBigRubles">{totalSumm[0]}<sup>{totalSumm[1]}</sup></div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HistoryItem;