import React, {Component} from 'react';
import HistoryItem from './HistoryItem';
import axios from "axios";

class History extends Component {
    state = {
        list: []
    };

    componentDidMount() {
        this.props.loaderStart('.right-content');
        axios.get(`http://localhost:3000/json/historyList.json`)
            .then(res => {
                this.props.loaderEnd('.right-content');
                const list = res.data;
                this.setState({ list });
            })
    }

    render(){
        let items = this.state.list.map(item => {
            let summ = item.summ ? item.summ.split(".") : false;
            let discount = item.discount ? item.discount.split(".") : false;
            let deliveryPrice = item.deliveryPrice ? item.deliveryPrice.split(".") : false;
            let totalSumm = item.totalSumm ? item.totalSumm.split(".") : false;
            return (
                <HistoryItem
                    key={item.index}
                    orderNumber={item.orderNumber}
                    orderDate={item.orderDate}
                    orderStatus={item.orderStatus}
                    basket={item.basket}
                    basketCount={item.basketCount}
                    summ={summ}
                    discount={discount}
                    deliveryPrice={deliveryPrice}
                    totalSumm={totalSumm}
                />
            );
        });
        return(
            <div className="historyOrder">
                <h2 className="h2HeadlineInterface historyOrder--title">Список заказов</h2>
                <div className="historyOrder--list">
                    {items}
                </div>
            </div>
        )
    }
}

export default History;