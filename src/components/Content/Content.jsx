import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import Bonus from './Bonus/Bonus';
import EditBonus from './Bonus/EditBonus';
import Favorites from './Favorites/Favorites';
import History from './History/History';
import AddressBook from './AddressBook/AddressBook';
import EditItem from './AddressBook/EditItem';

class Content extends Component {

    state = {
        addressBook: null,
        personalData: null
    };

    loaderStart(element) {
        let block = document.querySelector(element);
        if (block === undefined) {
            block = document.body;
            block.classList.add('loaderArea', 'active-body');
        } else {
            block.classList.add('loaderArea', 'active-loader');
        }
    }

    loaderEnd(element) {
        let block = document.querySelector(element);
        if (block === undefined) {
            block = document.body;
            block.classList.remove('active-body');
            setTimeout(function(){
                block.classList.remove('loaderArea');
            }, 350)
        } else {
            block.classList.remove('active-loader');
            setTimeout(function(){
                block.classList.remove('loaderArea');
            }, 350)
        }
    }

    setAddressBook = (data) => {
        this.setState({
            addressBook: data
        });
    }

    setPersonalData = (data) => {
        this.setState({
            personalData: data
        });
    }

    render(){
        return(
            <div className="right-content">
                <Route exact path="/bonus" render={() => <Bonus
                    loaderStart={this.loaderStart.bind(this)}
                    loaderEnd={this.loaderEnd.bind(this)}
                    setPersonalData={this.setPersonalData}
                />} />
                <Route path="/bonus/edit" render={() => <EditBonus
                    loaderStart={this.loaderStart.bind(this)}
                    loaderEnd={this.loaderEnd.bind(this)}
                    personalData={this.state.personalData}
                />} />
                <Route exact path="/address-book" render={() =>
                    <AddressBook
                        loaderStart={this.loaderStart.bind(this)}
                        loaderEnd={this.loaderEnd.bind(this)}
                        setAddressBook={this.setAddressBook}
                    />}
                />
                <Route path="/favorites" render={() => <Favorites loaderStart={this.loaderStart.bind(this)} loaderEnd={this.loaderEnd.bind(this)} />} />
                <Route path="/history" render={() => <History loaderStart={this.loaderStart.bind(this)} loaderEnd={this.loaderEnd.bind(this)}/>} />
                <Route path="/subscribes" component={Test2} />
                <Route path="/invite-friend" component={Test3} />
                <Route path="/exit" component={Test1} />
                <Route path="/address-book/edit/:addressIndex" render={() => <EditItem loaderStart={this.loaderStart.bind(this)} loaderEnd={this.loaderEnd.bind(this)} addressBook={this.state.addressBook} />}/>
                <Route path="/address-book/add" render={() => <EditItem loaderStart={this.loaderStart.bind(this)} loaderEnd={this.loaderEnd.bind(this)} />}/>
            </div>
        )
    }
}

export default Content;


class Test1 extends Component {
    render(){
        return(
            <div>
                test1
            </div>
        )
    }
}

class Test2 extends Component {
    render(){
        return(
            <div>
                test2
            </div>
        )
    }
}

class Test3 extends Component {
    render(){
        return(
            <div>
                test3
            </div>
        )
    }
}
