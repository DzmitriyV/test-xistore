import React, {Component} from 'react';
import AddressBookItem from './AddressBookItem';
import axios from "axios";
import {NavLink} from 'react-router-dom';

class AddressBook extends Component {
    state = {
        list: []
    };

    componentDidMount() {
        this.props.loaderStart('.right-content');
        axios.get(`http://localhost:3000/json/addressBook.json`)
            .then(res => {
                this.props.loaderEnd('.right-content');
                const list = res.data;
                this.setState({ list });
            });
    }

    render(){
        let items = this.state.list.map(item => {
            let name = item.name.split(" ");
            return (
                <AddressBookItem
                    key={item.index}
                    index={item.index}
                    name={name}
                    city={item.city}
                    address={item.address}
                    tel={item.tel}
                    email={item.email}
                    setAddressBook={this.props.setAddressBook}
                />
            );
        });

        return(
            <div className="addressSaved addressBook">
                <h2 className="h2HeadlineInterface addressSaved--title">Список адресов</h2>
                <div className="addressSaved--add">
                    <NavLink className="addressSaved--btn"  to="/address-book/add"></NavLink>
                    <div className="addressSaved--desc">добавить адрес</div>
                </div>
                <div className="addressSaved--list">{items}</div>
                <div className="popup--item popupQuestion standartAnimation" data-block="1">
                    <button className="popup--close js-popup-close"></button>
                    <div className="head--title">Удаление адреса</div>
                    <div className="popupQuestion--description">Вы точно хотите удалить адрес?</div>
                    <div className="popupQuestion--btns">
                        <a className="popupQuestion--btn contour--button js-popup-close" href="#"><span>Отмена</span></a>
                        <a className="popupQuestion--btn contour--button volume js-confirm-remove" href="#"><span>Удалить</span></a>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddressBook;