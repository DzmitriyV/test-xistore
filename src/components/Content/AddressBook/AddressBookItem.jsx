import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

class AddressBookItem extends Component {

    render(){
        let {index, name, city, address, tel, email} = this.props;

        return(
            <div className="addressSaved--item" data-index={index}>
                <div className="item--wrap">
                    <div className="addressSaved--user">
                        <div className="user--icon"></div>
                        <div className="user--desc">{name[0]}<br/> {name[1]}</div>
                    </div>
                    <div className="addressSaved--location">
                        <div className="location--icon"></div>
                        <div className="location--desc">{city}<br/> {address}</div>
                    </div>
                    <div className="addressSaved--phone">
                        <div className="phone--icon"></div>
                        <div className="phone--desc">{tel}</div>
                    </div>
                    <div className="addressSaved--options">
                        <NavLink to={`/address-book/edit/${index}`} onClick={() => {this.props.setAddressBook({index, name, city, address, tel, email})}}>Редактировать</NavLink>
                        <a className="js-click-popup remove-btn" data-click="1" href="#">Удалить</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddressBookItem;