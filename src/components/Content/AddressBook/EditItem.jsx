import React, {Component} from 'react';
import axios from 'axios';
import {withRouter, NavLink} from 'react-router-dom';

class EditItem extends Component {
    state = {
        addressBook: null,
    };

    setValue(element) {
        let newAddressBook = this.state.addressBook;
        newAddressBook[element.target.id] = element.target.value;
        this.setState({
            addressBook: newAddressBook
        });
    }

    componentDidMount() {
        if(this.props.addressBook) {
            this.setState({addressBook: this.props.addressBook})
        } else if(this.props.match) {
            this.props.loaderStart('.right-content');
            axios.get(`http://localhost:3000/json/addressBook.json`)
                .then(res => {
                    this.props.loaderEnd('.right-content');
                    const addressBook = res.data[this.props.match.params.addressIndex - 1];
                    this.setState({addressBook});
                });
        }

    }

    render(){
        let {addressBook} = this.state;
        if(addressBook && addressBook.name && typeof addressBook.name === "object") {
            addressBook.name = addressBook.name.join(" ");
        }

        return(
            <div className="inputsBlock personal-info">
                <h2 className="h2HeadlineInterface inputsBlock--title">Адрес</h2>
                <div className="inputsBlock--list">
                    <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                        <input id="name" className="text-input" type="text" placeholder="Фамилия и имя" value={addressBook ? addressBook.name : ""} onChange={this.setValue.bind(this)}/>
                        <label htmlFor="name" className="text-input--label">Фамилия и имя</label>
                    </div>
                    <div className="inputsBlock--item text-input--wrap full" data-required data-name="email">
                        <input id="email" className="text-input" type="text" placeholder="E-mail" value={addressBook ? addressBook.email : ""} onChange={this.setValue.bind(this)}/>
                        <label htmlFor="email" className="text-input--label">E-mail</label>
                        <div className="text-input--error">Текст ошибки появится тут</div>
                    </div>
                    <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                        <input id="tel" className="text-input" type="text" placeholder="Контактный номер телефона" value={addressBook ? addressBook.tel : ""} onChange={this.setValue.bind(this)}/>
                        <label htmlFor="phone" className="text-input--label">Контактный номер телефона</label>
                        <div className="text-input--error">Текст ошибки появится тут</div>
                    </div>
                    <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                        <input id="city" className="text-input" type="text" placeholder="Местоположение" value={addressBook ? addressBook.city : ""} onChange={this.setValue.bind(this)}/>
                        <label htmlFor="city" className="text-input--label">Местоположение</label>
                        <div className="text-input--error">Текст ошибки появится тут</div>
                    </div>
                    <div className="inputsBlock--item textarea-input--wrap" data-required data-name="length">
                        <textarea rows="3" id="address" className="textarea-input" placeholder="Улица, номер дома, квартира, подъезд" value={addressBook ? addressBook.address : ""} onChange={this.setValue.bind(this)}></textarea>
                        <label htmlFor="address" className="textarea-input--label">Улица, номер дома, квартира, подъезд</label>
                        <div className="textarea-input--error">Текст ошибки появится тут</div>
                    </div>
                    <div className="inputsBlock--item">
                        <NavLink to={`/address-book`} className="inputsBlock--btn contour--button" ><span>Отмена</span></NavLink>
                        <a className="inputsBlock--btn contour--button volume js-validate" href="#"><span>Сохранить</span></a>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(EditItem);