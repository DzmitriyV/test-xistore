import React, {Component} from 'react';
import axios from "axios";
import {NavLink} from 'react-router-dom';

class EditBonus extends Component {
    state = {
        personalData: null
    };

    setValue(element) {
        let newPersonalData = this.state.personalData;
        newPersonalData[element.target.id] = element.target.value;
        this.setState({
            personalData: newPersonalData
        });
    }

    componentDidMount() {
        if(this.props.personalData) {
            this.setState({personalData: this.props.personalData})
        } else {
            this.props.loaderStart('.right-content');
            axios.get(`http://localhost:3000/json/personalData.json`)
                .then(res => {
                    this.props.loaderEnd('.right-content');
                    const personalData = res.data[0];
                    this.setState({personalData});
                });
        }

    }

    render(){
        let {personalData} = this.state;
        if(personalData && personalData.name && typeof personalData.name === "object") {
            personalData.name = personalData.name.join(" ");
        }


        return(
            <>
                <div className="accountDescription">
                    <div className="accountDescription--avatar">
                        <picture>
                            <source type="image/webp"
                                    srcSet="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-user.webp 1x,
                                                        http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-userx2.webp 2x"/>
                                <img srcSet="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-userx2.jpg 2x"
                                     src="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-user.jpg"
                                     alt="Просто изображение"/>
                        </picture>
                    </div>
                    <div className="accountDescription--info">
                        <div className="accountDescription--name">Сергей Шнуров</div>
                        <div className="accountDescription--email">shnur@mail.ru</div>
                    </div>
                </div>
                <div className="inputsBlock personal-info">
                    <h2 className="h2HeadlineInterface inputsBlock--title">Редактирование личных даных</h2>
                    <div className="inputsBlock--list">
                        <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                            <input id="surname" className="text-input" type="text" placeholder="Фамилия" value={personalData ? personalData.surname : ""} onChange={this.setValue.bind(this)} />
                                <label htmlFor="surname" className="text-input--label">Фамилия</label>
                                <div className="text-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                            <input id="name" className="text-input" type="text" placeholder="Имя" value={personalData ? personalData.name : ""} onChange={this.setValue.bind(this)} />
                                <label htmlFor="name" className="text-input--label">Имя</label>
                                <div className="text-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                            <input id="patronymic" className="text-input" type="text" placeholder="Отчество" value={personalData ? personalData.patronymic : ""} onChange={this.setValue.bind(this)} />
                                <label htmlFor="patronymic" className="text-input--label">Отчество</label>
                                <div className="text-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item text-input--wrap full" data-required data-name="length">
                            <input id="tel" className="text-input js-phone" type="text" placeholder="Телефон" value={personalData ? personalData.tel : ""} onChange={this.setValue.bind(this)} />
                                <label htmlFor="phone" className="text-input--label">Телефон</label>
                                <div className="text-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item select--wrap full">
                            <select id="sex" className="select" onInput={this.setValue.bind(this)}>
                                <option value="Мужской" selected={personalData && personalData.sex === "Мужской" ? true : false}>Мужской</option>
                                <option value="Женский" selected={personalData && personalData.sex === "Женский" ? true : false}>Женский</option>
                            </select>
                            <label className="select--label">Пол</label>
                        </div>
                        <div className="inputsBlock--item text-input--wrap full">
                            <input id="calendar" className="text-input" type="text" placeholder="Дата рождения" value="Битриксовый календарь"/>
                                <label htmlFor="calendar" className="text-input--label">Дата рождения</label>
                                <div className="text-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item password-input--wrap full" data-required data-name="length">
                            <input id="password" className="password-input" type="password" placeholder="Новый пороль" />
                                <label htmlFor="password" className="password-input--label">Новый пороль</label>
                                <a className="password-input--btn js-password-input--btn" href="#"></a>
                                <div className="password-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item password-input--wrap open full" data-required data-name="password">
                            <input id="confirm" className="password-input" type="text" placeholder="Подтверждение нового пороля" />
                                <label htmlFor="confirm" className="password-input--label">Подтверждение нового пороля</label>
                                <a className="password-input--btn js-password-input--btn" href="#"></a>
                                <div className="password-input--error">Текст ошибки появится тут</div>
                        </div>
                        <div className="inputsBlock--item">
                            <NavLink to={`/bonus`} className="inputsBlock--btn contour--button" ><span>Отмена</span></NavLink>
                            <a className="inputsBlock--btn contour--button volume js-validate" href="#"><span>Сохранить</span></a>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default EditBonus;