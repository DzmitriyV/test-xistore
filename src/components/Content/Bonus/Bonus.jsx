import React, {Component} from 'react';
import axios from "axios";
import {NavLink} from 'react-router-dom';

class Bonus extends Component {
    state = {
        personalData: {}
    };

    componentDidMount() {
        this.props.loaderStart('.right-content');
        axios.get(`http://localhost:3000/json/personalData.json`)
            .then(res => {
                this.props.loaderEnd('.right-content');
                const personalData = res.data[0];
                this.setState({ personalData });
            });
    }

    render(){
        let {index, surname, name, patronymic, tel, email, sex, birthday, cardNumber, cardBalance} = this.state.personalData;
        let summ = cardBalance ? cardBalance.split(".") : false;

        return(
            <>
                <div className="accountDescription">
                    <div className="accountDescription--avatar">
                        <picture>
                            <source type="image/webp"
                                    srcSet="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-user.webp 1x,
                                                        http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-userx2.webp 2x" />
                                <img srcSet="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-userx2.jpg 2x"
                                     src="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/ava-user.jpg"
                                     alt="Просто изображение" />
                        </picture>
                    </div>
                    <div className="accountDescription--info">
                        <div className="accountDescription--name">{name} {surname}</div>
                        <div className="accountDescription--email">{email}</div>
                    </div>
                </div>
                <div className="accountBonusCard">
                    <h2 className="h2HeadlineInterface accountBonusCard--title">Бонусная карта</h2>
                    <div className="accountBonusCard--body">
                        <div className="accountBonusCard--row">
                            <div className="accountBonusCard--img">
                                <img src="http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/logoXistore-vertical.svg"/>
                            </div>
                            <div className="accountBonusCard--price priceBigRubles">
                                {summ[0]}<sup>{summ[1]}</sup>
                            </div>
                        </div>
                        <div className="accountBonusCard--row">
                            <div className="accountBonusCard--number">
                                {cardNumber}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="accountPersonalInfo">
                    <h2 className="h2HeadlineInterface accountPersonalInfo--title">Персональная информация</h2>
                    <div className="accountPersonalInfo--list">
                        <div className="accountPersonalInfo--item">
                            <div className="accountPersonalInfo--item-prop">ID клиента</div>
                            <div className="accountPersonalInfo--item-value">{index}</div>
                        </div>
                        <div className="accountPersonalInfo--item">
                            <div className="accountPersonalInfo--item-prop">E-mail</div>
                            <div className="accountPersonalInfo--item-value">{email}</div>
                        </div>
                        <div className="accountPersonalInfo--item">
                            <div className="accountPersonalInfo--item-prop">Телефон</div>
                            <div className="accountPersonalInfo--item-value">{tel}</div>
                        </div>
                        <div className="accountPersonalInfo--item">
                            <div className="accountPersonalInfo--item-prop">Пол</div>
                            <div className="accountPersonalInfo--item-value">{sex}</div>
                        </div>
                        <div className="accountPersonalInfo--item">
                            <div className="accountPersonalInfo--item-prop">День рождения</div>
                            <div className="accountPersonalInfo--item-value">{birthday}</div>
                        </div>
                        <NavLink to={`/bonus/edit`} className="accountPersonalInfo--btn contour--button" onClick={() => {this.props.setPersonalData({index, surname, name, patronymic, tel, sex, birthday})}}><span>Редактировать личные данные</span></NavLink>
                    </div>
                </div>
            </>
        )
    }
}

export default Bonus;