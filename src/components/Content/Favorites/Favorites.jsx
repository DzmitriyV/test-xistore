import React, {Component} from 'react';
import CardProduct from "../CardProduct";
import axios from 'axios';

class Favorites extends Component {
    state = {
        list: []
    };

    componentDidMount() {
        this.props.loaderStart('.right-content');
        axios.get(`http://localhost:3000/json/favoritesList.json`)
            .then(res => {
                this.props.loaderEnd('.right-content');
                const list = res.data;
                this.setState({ list });
            })
    }

    render(){
        let items = this.state.list.map(item => {
            return (
                <CardProduct
                    key={item.index}
                    index={item.index}
                    name={item.name}
                    link={item.link}
                    price={item.price}
                    oldPrice={item.oldPrice}
                    popupIndex={item.popupIndex}
                    imgName={item.imgName}
                    extraClass="favoritesList--item"
                    removable={true}
                />
            );
        });
        return(
            <div className="favoritesList">
                <h2 className="h2HeadlineInterface favoritesList--title">Список избранных товаров</h2>
                {items}
                <div className="popup--item popupQuestion standartAnimation" data-block="1">
                    <button className="popup--close js-popup-close"></button>
                    <div className="head--title">Удаление адреса</div>
                    <div className="popupQuestion--description">Вы точно хотите удалить адрес?</div>
                    <div className="popupQuestion--btns">
                        <a className="popupQuestion--btn contour--button" href="#"><span>Отмена</span></a>
                        <a className="popupQuestion--btn contour--button volume js-confirm-remove"
                           href="#"><span>Удалить</span></a>
                    </div>
                </div>
            </div>
        )
    }
}

export default Favorites;