import React, {Component} from 'react';

class CardProduct extends Component {
    render(){
        let {index, name, link, price, oldPrice, popupIndex, imgName, extraClass, removable, count, totalPrice} = this.props;

        price = price ? price.split(".") : false;
        oldPrice = oldPrice ? oldPrice.split(".") : false;
        totalPrice = totalPrice ? totalPrice.split(".") : false;

        let oldPriceBlock = oldPrice ? (
            <div className="old-price priceSmall priceSmall--old">
                {oldPrice[0]}<sup>{oldPrice[1]}</sup>
            </div>
        ) : "";

        let countWrap = totalPrice ? (
            <div className="cardProduct--countWrap">
                <div className="cardProduct--count">
                    {`${count}x`}
                </div>
                <div className="cardProduct--total-amount priceBigRubles">
                    {totalPrice[0]}<sup>{totalPrice[1]}</sup>
                </div>
            </div>
        ) : "";

        let removeProductBlock = removable ? (
            <a className="cardProduct--trash js-click-popup remove-btn" data-click={popupIndex} href="#"></a>
        ) : "";

        extraClass = extraClass ? `cardProduct ${extraClass}` : "cardProduct";

        return(
            <div className={extraClass} data-index={index}>
                <div className="cardProduct--img">
                    <a href={link}>
                        <picture>
                            <source type="image/webp" srcSet={`http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}.webp 1x,
                                                    http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}x2.webp 2x`}/>
                            <img srcSet={`http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}x2.png 2x`} src={`http://html.dev-bitrix.by/Leonchik_R/2019/pravki/xistore/xistore-mobile/img/${imgName}.png`}
                                 alt="Просто изображение"/>
                        </picture>
                    </a>
                </div>
                <div className="cardProduct--content">
                    <div className="cardProduct--name">
                        <a href={link}>{name}</a>
                    </div>
                    <div className="cardProduct--price">
                        <div className="new-price priceSmall priceSmall--discount">
                            {price[0]}<sup>{price[1]}</sup>
                        </div>
                        {oldPriceBlock}
                    </div>
                    {countWrap}
                    {removeProductBlock}
                </div>
            </div>
        )
    }
}

export default CardProduct;