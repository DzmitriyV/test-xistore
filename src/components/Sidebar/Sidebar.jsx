import React, {Component} from 'react';
import SidebarItem from './SidebarItem';
import axios from 'axios';

class Sidebar extends Component {
	state = {
		list: []
	};

	componentDidMount() {
		axios.get(`http://localhost:3000/json/sidebarList.json`)
			.then(res => {
				const list = res.data;
				this.setState({ list });
			})
	}

	render(){
        let items = this.state.list.map(item => {
            return (
                <SidebarItem
                    key={item.ITEM_INDEX}
                    name={item.TEXT}
                    link={item.LINK}
                    selected={item.SELECTED}
					imgName={item.svg}
					price={item.price}
					note={item.note}
					svgClass={item.svgClass}
                />
            );
        });
		return(
			<div className="accountMenu sidebar">
                {items}
			</div>
		)
	}
}

export default Sidebar;