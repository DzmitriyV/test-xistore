import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

class SidebarItem extends Component {
	render(){
        let {name, link, imgName, price, note, svgClass} = this.props;
        let itemPrice = (
            <div className="item--price priceBigRubles">0<sup>00</sup></div>
        );
        let itemNote = (
            <div className="item--note">X XXXXXX XXXXXX</div>
        );

		return(
            <NavLink className="accountMenu--item" activeClassName="checked" to={link}>
                <div className="item--wrap">
                    <div className={`item--icon ${svgClass}`}>
                        <svg>
                            <use xlinkHref={`img/menu-svg/1.svg#${imgName}`} />
                        </svg>
                    </div>
                    <div className="item--name">{name}
                        {note ? itemNote : ""}
                    </div>
                    {price ? itemPrice : ""}

                </div>
            </NavLink>
		)
	}
}

export default SidebarItem;