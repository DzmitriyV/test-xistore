import React, { Component } from 'react';
import Sidebar from './components/Sidebar/Sidebar';
import Content from './components/Content/Content';
import {BrowserRouter} from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="container with-sidebar">
                    <div className="section--title">Личный кабинет</div>
                    <Content />
                    <Sidebar />
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
